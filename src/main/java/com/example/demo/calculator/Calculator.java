package com.example.demo.calculator;

import com.example.demo.model.Line;
import lombok.extern.java.Log;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.List;

import static java.lang.Math.pow;
import static java.lang.Math.sqrt;

public class Calculator {
    private static final Logger LOG = LoggerFactory.getLogger(Calculator.class);

    public static Line calculate(Line line) {
        if(line.getColumn1().contains("1Ф") || line.getColumn1().contains("1ф")) {
            line.setColumn4(line.getColumn2() * line.getColumn3() * 3);
        }
        line.setColumn6Tg((Math.sqrt(1 - pow(line.getColumn6Cos(), 2))) / line.getColumn6Cos());
        line.setColumn7(line.getColumn5() * line.getColumn4());
        line.setColumn8(line.getColumn7() * line.getColumn6Tg());
        line.setColumn9(line.getColumn2() * Math.pow(line.getColumn3(), 2));
        return line;
    }

    public static Line summarise(List<Line> lines) {
        Line line = new Line();

        Double sum2 = lines.stream().mapToDouble(Line::getColumn2).sum();
        line.setColumn2(sum2);
        Double sum4 = lines.stream().mapToDouble(Line::getColumn4).sum();
        line.setColumn4(sum4);
        Double sum7 = lines.stream().mapToDouble(Line::getColumn7).sum();
        line.setColumn5(sum7/sum4);
        Double sum8 = lines.stream().mapToDouble(Line::getColumn8).sum();
        Double tg = sum8/sum7;
        line.setColumn6Tg(tg);
        line.setColumn6Cos(Math.sqrt(1/(1+Math.pow(tg, 2))));
        line.setColumn7(sum7);
        line.setColumn8(sum8);
        Double sum9 = lines.stream().mapToDouble(Line::getColumn9).sum();
        line.setColumn9(sum9);
        line.setColumn10(pow(line.getColumn4(), 2)/line.getColumn9());
        line.setColumn13(line.getColumn10() <= 10 ? 1.1 * line.getColumn8() : line.getColumn8());
        if (null != line.getColumn11()) {
            line.setColumn12(line.getColumn11() * line.getColumn7());
            line.setColumn14(Math.sqrt(Math.pow(line.getColumn12(), 2) + Math.pow(line.getColumn13(), 2)));
            line.setColumn15(line.getColumn14()/(sqrt(3) * 380));
        }

        return line;
    }

    public static Line summarise(Line sumLine) {

        if (null != sumLine.getColumn11()) {
            LOG.info(sumLine.getColumn11().toString());
            LOG.info(sumLine.getColumn7().toString());
            sumLine.setColumn12(sumLine.getColumn11() * sumLine.getColumn7());
            sumLine.setColumn14(Math.sqrt(Math.pow(sumLine.getColumn12(), 2) + Math.pow(sumLine.getColumn13(), 2)));
            sumLine.setColumn15(sumLine.getColumn14() /( sqrt(3) * 380));
        }

        return sumLine;
    }
}
