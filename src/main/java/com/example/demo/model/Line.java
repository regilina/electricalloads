package com.example.demo.model;

import lombok.*;

import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.validation.constraints.NotNull;

@Getter
@Setter
@ToString
@NoArgsConstructor
@AllArgsConstructor
public class Line {

    private Integer id;
    private String column1;
    private Double column2;
    private Double column3;
    private Double column4;
    private Double column5;
    private Double column6Cos;
    private Double column6Tg;
    private Double column7;
    private Double column8;
    private Double column9;
    private Double column10;
    private Double column11;
    private Double column12;
    private Double column13;
    private Double column14;
    private Double column15;
    private Double column16;


}