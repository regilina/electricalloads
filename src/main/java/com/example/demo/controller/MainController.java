package com.example.demo.controller;

import com.example.demo.calculator.Calculator;
import com.example.demo.model.Line;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import javax.servlet.http.HttpSession;
import java.util.LinkedList;
import java.util.List;

@Controller
public class MainController {
    private static final Logger LOG = LoggerFactory.getLogger(MainController.class);

    @GetMapping(path={ "/home"})
    public String home (HttpSession session, Model model) {
        if(null == session.getAttribute("lines")) {
            session.setAttribute("lines", new LinkedList<Line>());
        }
        model.addAttribute("line", new Line());
        model.addAttribute("lines", session.getAttribute("lines"));
        session.setAttribute("sumLine", false);
        return "home";
    }

    @GetMapping(path={ "/summarise"})
    public String summarise (HttpSession session, Model model) {
        List<Line> lines = (List<Line>) session.getAttribute("lines");
        Line sumLine = (Calculator.summarise(lines));
        model.addAttribute("line", new Line());
        model.addAttribute("lines", lines);
        model.addAttribute("sumLine", sumLine);
        session.setAttribute("sumLine", true);
        session.setAttribute("sumRow", sumLine);
        return "home";
    }

    @PostMapping(path={ "/summarise"})
    public String summarise (Line sumLine, HttpSession session, Model model) {
        LOG.info(     sumLine.toString());
        List<Line> lines = (List<Line>) session.getAttribute("lines");
        ( (Line)session.getAttribute("sumRow")).setColumn11(sumLine.getColumn11());
        Line newSumLine = (Calculator.summarise((Line)session.getAttribute("sumRow")));
        model.addAttribute("line", new Line());
        model.addAttribute("lines", lines);
        model.addAttribute("sumLine", newSumLine);
        session.setAttribute("sumLine", true);

        return "home";
    }

    @PostMapping(path="/calculate")
    public String save (Line line, HttpSession session, Model model) {

        LOG.info(     line.toString());
        List<Line> lines = (List<Line>) session.getAttribute("lines");
        if ((Boolean)session.getAttribute("sumLine")) {
            lines.clear();
            session.setAttribute("sumLine", false);
        }
        lines.add(Calculator.calculate(line));
        model.addAttribute("line", new Line());
        model.addAttribute("lines", lines);
        return "home";
    }

    @GetMapping(path="/delete/byId/{id}")
    public String byId (@PathVariable("id") Integer id, HttpSession session , Model model) {
        ((List<Line>)session.getAttribute("lines")).removeIf(a -> a.hashCode() == (id));

        model.addAttribute("line", new Line());
        model.addAttribute("lines", session.getAttribute("lines"));

    return "home";
    }

    }